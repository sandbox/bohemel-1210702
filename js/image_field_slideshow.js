(function($) {
  Drupal.behaviors.imageFieldSlideshowBehavior = {
    attach: function() {
      $.each(Drupal.settings.imageFieldSlideshow, function(i, settings) {
        settings.params.pagerAnchorBuilder = function(index, DOMelement) {return '<a href="#"><img src="' + $('#' + settings.id + '-thumbnails #thumb_' + index + ' img').attr('src')  + '" /></a>'; };
        $('#' + settings.id).cycle(settings.params);  
      });
    }
  };
})(jQuery);
